# Geocoding Microservice #

Microservice to perform an address lookup using the Goolgle Geocoding API

http://geocode-microservice.herokuapp.com

### What is this repository for? ###

* Pass an address or postcode in to a URL via query string to receive a geo json object.
* Alternatively, pass in an IP address to get an approximate location. (Specifying an IP address will alwways override an address entry)
* Version 1.0

### How do I get set up? ###

* Clone repo
* Install deps
* Rename .env-sample to .env and add your own Google API key
* NPM start

### Params ###

* address [postcode, street name, town etc]
* ip [ip address]

### Example usage ###


```
#!js

axios.get('http://geocode-microservice.herokuapp.com', {
    params: {
      address: "10 Downing St, Westminster, London SW1A 2AA, UK",
    }
  })
  .then(function (response) {
    console.log(response.data);
  })
  .catch(function (error) {
    console.log(error);
  });  
```

### Result ###


```
#!json

{
    "formattedAddress": "10 Downing St, Westminster, London SW1A 2AA, UK",
    "latitude": 51.5033635,
    "longitude": -0.1276248,
    "extra": {
        "googlePlaceId": "ChIJRxzRQcUEdkgRGVaKyzmkgvg",
        "confidence": 1,
        "premise": null,
        "subpremise": null,
        "neighborhood": "Westminster",
        "establishment": null
    },
    "administrativeLevels": {
        "level2long": "Greater London",
        "level2short": "Greater London",
        "level1long": "England",
        "level1short": "England"
    },
    "streetNumber": "10",
    "streetName": "Downing Street",
    "country": "United Kingdom",
    "countryCode": "GB",
    "zipcode": "SW1A 2AA",
    "provider": "google"
}
```