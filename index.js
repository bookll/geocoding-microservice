require('dotenv').config()

const {
    send
} = require('micro')
const url = require('url')
const NodeGeocoder = require('node-geocoder');
const iplocation = require('iplocation');

const options = {
    provider: 'google',
    apiKey: process.env.KEY
};

module.exports = function(req, res) {

    // Pass true as the second argument to also parse the query string using the querystring module.    

    const path = url.parse(req.url, true);
    const address = path.query.address || "10 Downing St, Westminster, London SW1A 2AA, UK";
    const ip = path.query.ip || false;

    let geocoder = NodeGeocoder(options);

    if (ip) {

        iplocation(ip).then(data => {

            const geo = JSON.stringify(data[0], null, 4);
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Content-Type', 'application/json');
            res.write(geo);
            send(res, 200);

            console.log(geo);

        }).catch(function(err) {
            console.log(err);
        })

    } else {

        geocoder.geocode(address).then(data => {

            const geo = JSON.stringify(data[0], null, 4);

            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Content-Type', 'application/json');
            res.write(geo);
            send(res, 200);

            console.log(geo);

        }).catch(function(err) {
            console.log(err);
        })

    }
}